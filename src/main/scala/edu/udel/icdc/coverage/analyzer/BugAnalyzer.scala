package edu.udel.icdc.coverage.analyzer

import edu.udel.icdc.utils.Types._
import com.ibm.wala.classLoader.IMethod
import edu.udel.icdc.utils.ImplicitConversions.MapWrapper
import edu.udel.icdc.coverage.reader.BugInfo

case class BugTestProfile(test: IMethod, ds: Map[IMethod, Set[Int]], is: Map[IMethod, Set[Int]])

case class BugAnalyzer(profiles: List[BugTestProfile]) {
	val rec = scala.collection.mutable.Map[IMethod, (Int,Int)]().withDefaultValue((0,0))
	var exception = List[String]()
	def analyzeBugPerProfile(bi: BugInfo, p: BugTestProfile) = {
		if (p.test.getDeclaringClass.toString contains "TimeZoneNameRule") println(p)
		bi.bugs foreach {
			case (t, s) => {
				val di = p.ds(t) intersect s
				if (di.nonEmpty) {
					rec.update(p.test, (rec(t)._1+1, rec(t)._2))
					if (!(bi.tests contains p.test))
						exception ::= ("other test do direct::" ++ p.test.toString ++ t.toString ++ di.toString)
//					println(p.test, di)
				}
				val ii = p.is(t) intersect s
				if (ii.nonEmpty) {
					rec.update(p.test, (rec(t)._1, rec(t)._2+1))
					if (bi.tests contains p.test)
						exception ::= ("failing test do indirect::" ++ p.test.toString ++ t.toString ++ ii.toString)
				}
			}
		}
	}
	
	def analyzeBug(bi: BugInfo) = profiles foreach { p => analyzeBugPerProfile(bi, p)}
	
	def count(bi: BugInfo) = {
		val failingTests = ((0,0) /: bi.tests) {
			case (acc, m) => (acc._1+rec(m)._1, acc._2+rec(m)._2)
		}
		
		val others = ((0,0) /: profiles) {
			case (acc, p) => 
				if (bi.tests contains p.test) 
					acc
				else
					(acc._1+rec(p.test)._1, acc._2+rec(p.test)._2)
		}

		(failingTests, others)
	}
}

object BugAnalyzer {
	def apply(directs: ShallowMap, db: TestSuiteCoverageInfo) = {
		val all = scala.collection.mutable.Map[IMethod, Set[Int]]().withDefaultValue(Set.empty)

		db foreach {
			case (t, v) =>
				v foreach {
					case (k, vv) => all.update(k, all(k) union vv)
				}
		}

		val totLines = all.cSize
		//_1 direct, _2 indirect
		val splitted = db map { case (t, md) => t -> (md partition { case (m, s) => directs.getOrElse(t, List()).toList contains m }) }

		//		val priQueue = new PriorityQueue[TestProfile]()
		//
		val profiles = splitted map {
			case (t, (lds, lis)) =>
				new BugTestProfile(t, lds.withDefaultValue(Set.empty), lis.withDefaultValue(Set.empty))
		}
		
		new BugAnalyzer(profiles.toList)

	}
}