package edu.udel.coco.main
import edu.udel.coco.callgraph.MyCallgraph
import edu.udel.coco.analyzer.ProfileStore

object test2 {
val subject = "commons-cli"                       //> subject  : String = commons-cli
val sa = SubjectRunner(subject)                   //> sa  : edu.udel.coco.main.SubjectRunner = SubjectRunner(commons-cli,edu.illin
                                                  //| ois.wala.ipa.callgraph.AnalysisOptions@143369c7,<Primordial,Ljava/lang/Objec
                                                  //| t>:<Primordial,Lsun/java2d/loops/RenderCache>,<Primordial,Lcom/sun/corba/se/
                                                  //| impl/orb/ParserTable$10>,<Primordial,Lcom/sun/xml/internal/ws/client/WSServi
                                                  //| ceDelegate$4>,<Primordial,Ljava/sql/Driver>,<Primordial,Lsun/swing/plaf/synt
                                                  //| h/SynthIcon>,<Primordial,Ljavax/sound/midi/SoundbankResource>,<Primordial,Lj
                                                  //| ava/lang/Enum>,<Primordial,Ljava/lang/Throwable>,<Primordial,Ljava/util/Reso
                                                  //| urceBundle>,<Primordial,Ljava/security/cert/CertPath$CertPathRep>,<Primordia
                                                  //| l,Ljavax/security/auth/login/LoginContext$3>,<Primordial,Lcom/sun/xml/intern
                                                  //| al/bind/Util>,<Primordial,Lcom/sun/org/apache/xerces/internal/impl/xpath/reg
                                                  //| ex/Token>,<Primordial,Ljavax/accessibility/AccessibleKeyBinding>,<Primordial
                                                  //| ,Lsun/awt/X11/XMenuItemPeer>,<Primordial,Ljavax/swing/AbstractAction>,<Primo
                                                  //| rdial,Ljavax/xml/bind/an
                                                  //| Output exceeds cutoff limit.
val write = sa.profileOneSubject                  //> List(<Application,Lorg/apache/commons/cli/GnuParserTest>, <Application,Lorg/
                                                  //| apache/commons/cli/bug/BugCLI162Test>, <Application,Lorg/apache/commons/cli/
                                                  //| ParseRequiredTest>, <Application,Lorg/apache/commons/cli/OptionTest>, <Appli
                                                  //| cation,Lorg/apache/commons/cli/OptionBuilderTest>, <Application,Lorg/apache/
                                                  //| commons/cli/bug/BugCLI148Test>, <Application,Lorg/apache/commons/cli/Pattern
                                                  //| OptionBuilderTest>, <Application,Lorg/apache/commons/cli/bug/BugCLI71Test>, 
                                                  //| <Application,Lorg/apache/commons/cli/OptionGroupTest>, <Application,Lorg/apa
                                                  //| che/commons/cli/BasicParserTest>, <Application,Lorg/apache/commons/cli/bug/B
                                                  //| ugCLI133Test>, <Application,Lorg/apache/commons/cli/HelpFormatterTest>, <App
                                                  //| lication,Lorg/apache/commons/cli/bug/BugCLI18Test>, <Application,Lorg/apache
                                                  //| /commons/cli/CommandLineTest>, <Application,Lorg/apache/commons/cli/Argument
                                                  //| IsOptionTest>, <Application,Lorg/apache/commons/cli/OptionsTest>, <Applicati
                                                  //| on,Lorg/apache/commons/c
                                                  //| Output exceeds cutoff limit.-
val db = sa.readOneSubject
//val tests = sa.getTestsFromCallGraph

implicit val ch = sa.cha
implicit val opt = sa.opts
val cg = MyCallgraph(subject)
val directs = cg.directCalls
val indirects = cg.indirectCalls
val analyzer = ProfileStore(db, directs, indirects)
val records = analyzer.analyze
val one = records.toList(0)
val l = (0 /: records)(_ + _.uncovered.keys.toList.length) / records.toList.length
}