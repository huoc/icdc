package edu.udel.icdc.main

import org.junit.runner.Request
import org.junit.runner.JUnitCore

object SingleTestRunner {
	def main(args : Array[String]) {
        val classAndMethod = args(0).split("#")
        val request = Request.method(Class.forName(classAndMethod(0)), classAndMethod(1))

        val result = new JUnitCore().run(request)
//        System.exit(if (result.wasSuccessful) 0 else 1)
        System.exit(0)
	}
}
