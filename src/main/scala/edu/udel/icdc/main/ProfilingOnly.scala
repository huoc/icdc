package edu.udel.icdc.main

import edu.udel.icdc.coverage.analyzer.BasicAnalyzer
import edu.udel.icdc.mutation.MajorInfo
import edu.udel.icdc.mutation.MutationScoreAnalyzer
import edu.udel.icdc.main.Config.subjectHome

object ProfilingOnly {
	def main(args: Array[String]) = {
//		val subject = "commons-cli"
		val subject = args(0)
		val sa = SubjectRunner(subject)
		implicit val ch = sa.cha
		implicit val opt = sa.opts


		sa.profileOneSubject

	}
}