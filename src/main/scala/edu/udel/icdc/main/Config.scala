package edu.udel.icdc.main

import com.ibm.wala.types.MethodReference

object Config {

	val projectHome = "/home/huoc/Projects/icdcProject/tool/"
	val projectRoot = "/home/huoc/Projects/icdcProject/"
		
	val projectName = "/icdc/"
		
	val subjectHome = "/home/huoc/Projects/testassist/subjects/"

	val runnerCp = projectHome + projectName + "target/scala-2.10/classes/"

	val jacocoAgentPath = "/home/huoc/.ivy2/cache/org.jacoco/org.jacoco.agent/jars/org.jacoco.agent-0.7.2.201409121644-runtime.jar"
		
	val tracerPath = projectHome + "/tracer/tracer.so"
	
	val tracerSource = projectHome + "/tracer/tracer.c"
	
	val javaHome = "/usr/lib/jvm/java-7-openjdk-amd64/jre"

	def destFolder(subject: String, sub: String) = projectHome + "/exec/" + subject + "/" + sub + "/"
//	def destFolder(root: String, sub: String) = root + "/" + sub + "/"

	def destFile(subject: String, sub: String, m: MethodReference) =
		destFolder(subject, sub) + m.getDeclaringClass.getName.toString.replace("/", ".") + "." + m.getName.toString

	val mainClass = "edu.udel.icdc.main.SingleTestRunner"
}