//package edu.udel.icdc.main
//
//import edu.udel.icdc.mutation.MajorInfo
//import edu.udel.icdc.utils.Utils
//import edu.udel.icdc.coverage.analyzer.Analyzer
//import edu.udel.icdc.mutation.MutationScoreAnalyzer
//import edu.udel.icdc.coverage.reader.BugInfo
//import edu.udel.icdc.coverage.analyzer.BugAnalyzer
//import java.io.FileWriter
//import java.io.File
//
//object AnalysisOnly {
//	def main(args: Array[String]) = {
//		//val subject = "commons-lang"
//		val subject = args(0)
//		val profileFolder = args(1)
//		val sa = SubjectRunner(subject, profileFolder)
//		implicit val ch = sa.cha
//		implicit val opt = sa.opts
//
//		val bi = BugInfo(profileFolder)
//		println(bi)
//		sa.profileOneSubject(bi.tests)
//
//		println("reading coverage info...")
//		val db = sa.readOneSubject
//
//		println("reading trace files...")
//		val directs = sa.readTraceFiles
//
//		val ba = BugAnalyzer(directs, db)
//		ba.analyzeBug(bi)
//		
//		val count = ba.count(bi)
//		println(count)
//
//		val fw = new FileWriter(new File("/home/huoc/Projects/icdcProject/results/"+subject+".summary"), true)
//		try {
//			fw.write(profileFolder.split("/").drop(1).last.toString+"\t"+count.toString+"\n")
//		} finally fw.close()
//
//		val ex = new FileWriter(new File(profileFolder+"/exceptions"), true)
//		try {
//			ex.write(ba.exception mkString "\n")
//		} finally ex.close()
//		//		println(Utils.getTestMethods(subject).length)
//		//
//		//		val covs = List(0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9)
//		//		//val covs = List(0.25, 0.5, 0.75)
//		//		covs foreach { cov => {
//		//			val R = cov
//		//			val logBase = Config.destFolder(subject, "partial/"+R.toString+"/")+"/version"
//		//			var rAna = Analyzer(directs, db, Analyzer.minHeaping, true)
//		//			var cnt = 20
//		//
//		//			while (cnt > 10) {
//		//				Analyzer.generateSkippingTests(rAna, logBase, R, cnt)
//		//				rAna = Analyzer(directs, db, Analyzer.minHeaping, true)
//		//				cnt -= 1
//		//			}
//		//
//		///*			
//		//			val minAna = Analyzer(directs, db, Analyzer.minHeaping, false)
//		//			val maxAna = Analyzer(directs, db, Analyzer.maxHeaping, false)
//		//			Analyzer.generateSkippingTests(minAna, logBase, R, 6)
//		//			Analyzer.generateSkippingTests(maxAna, logBase, R, 7)
//		//			*/
//		//			}
//		//		}
//		//		
//		//		val (dcPool, icPool) = analyzer.cInfo
//		//		
//		//		println("gathering major info...")
//		//		val minfo = MajorInfo(subject)
//		//		println("analyze mutation info...")
//		//		val majorAnalyzer = MutationScoreAnalyzer(minfo, dcPool, icPool)
//		//		majorAnalyzer.toCsv(subject)
//		//		
//		//		println(majorAnalyzer)
//	}
//}
