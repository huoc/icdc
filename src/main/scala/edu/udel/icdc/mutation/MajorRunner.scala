package edu.udel.icdc.mutation

import edu.udel.icdc.main.Config.subjectHome

class MajorRunner(subject : String) extends Runnable {

	val command = subjectHome + subject + "/runMajor.sh"

	def run = command.lines
	
}