package edu.udel.icdc.utils

import java.io.File

import scala.collection.JavaConversions.iterableAsScalaIterable

import com.ibm.wala.classLoader.IBytecodeMethod
import com.ibm.wala.classLoader.IClass
import com.ibm.wala.classLoader.IMethod
import com.ibm.wala.ipa.cha.ClassHierarchy
import com.ibm.wala.types.ClassLoaderReference
import com.ibm.wala.types.MethodReference
import com.typesafe.config.ConfigFactory

import edu.illinois.wala.ipa.callgraph.AnalysisOptions
import edu.udel.icdc.main.Config.projectHome
import edu.udel.icdc.main.Config.projectName
import edu.udel.icdc.utils.ImplicitConversions.SubClassWrapper
import edu.udel.icdc.utils.Types.JUnitRunWith
import edu.udel.icdc.utils.Types.JUnitTest
import edu.udel.icdc.utils.Types.JUnitTestMethod

object Utils {

	def isSubOfJUnitOrRunWithJUnit(c: IClass)(implicit cha: ClassHierarchy) =
		(c.getName isSubOf JUnitTest) || ((c.getAnnotations map (_.getType)).toList contains JUnitRunWith)

	def getTestMethods(subject: String)(implicit cha: ClassHierarchy): Seq[MethodReference] = {
		val classes = Utils.getIClasses(cha).toList

		val fromMethods = classes flatMap { _.getDeclaredMethods } collect {
			case m: IBytecodeMethod => m
		} filter {
			m => (m.getAnnotations map (_.getType)).toList contains JUnitTestMethod
		} map { m => m.getReference }

		//		println(fromMethods.length)

		//		classes foreach {c => println(c.getAnnotations map (_.getType))}
		val (junits, nonjunits) = classes partition { _.getName isSubOf JUnitTest }
		val (runWithRoots, nonRootsNonJUnits) = nonjunits partition { c => (c.getAnnotations map (_.getType)).toList contains JUnitRunWith }
		val subsOfRoots = nonRootsNonJUnits filter { c => runWithRoots exists { r => c.getName isSubOf r.getName } }

		val testClasses = junits ++ runWithRoots ++ subsOfRoots

		//		val hint = Map("Sudoku" -> "sudoku", "commons-cli" -> "commons/cli", 
		//	"joda-time-2.2" -> "org/joda/time", "commons-lang" -> "commons/lang", "DataStructures" -> "datastructures")

		val subjectTestClasses = testClasses //filter { cls => cls.getName.toString.toLowerCase contains hint(subject)}

		def filteringMethods(ms: Seq[IMethod]) = ms collect {
			case m: IBytecodeMethod => m
		} filterNot { _.isStatic } filter { _.isPublic } filterNot { m => m.isClinit || m.isInit }

		val fromExplicitClasses = subjectTestClasses flatMap { c =>
			if (c.isAbstract) {
				for (
					sc <- cha.computeSubClasses(c.getReference()) filterNot { _.isAbstract };
					m <- filteringMethods(c.getDeclaredMethods.toList)
				) yield {
					//					println();println(sc);println(m.getName.toString);println(m.getDescriptor.toString);
					//				println(MethodReference.findOrCreate(sc.getReference, m.getName.toString, m.getDescriptor.toString));
					MethodReference.findOrCreate(sc.getReference, m.getName.toString, m.getDescriptor.toString)
				}

			} else {
				filteringMethods(c.getDeclaredMethods.toList) filterNot {
					_.getAnnotations contains JUnitTestMethod //remove duplicates against 'fromMethods'
				} map { _.getReference }
			}
		} filter {
			_.getDescriptor.toString equals "()V"
		} filter {
			_.getName.toString startsWith "test"
		}

		//		println(fromExplicitClasses.length)

		//		println(fromMethods.toSet intersect fromExplicitClasses.toSet)

		(fromMethods ++ fromExplicitClasses).toSet.toList
	}

	def getOptions(configName: String) = {

		val file = new File(projectHome + projectName + "/configs/" + configName)
		val confFile = ConfigFactory.parseFile(file)

		implicit val conf = ConfigFactory.load(confFile)
		AnalysisOptions()
	}

	def iS(cref: ClassLoaderReference): IClass => Boolean = {
		x: IClass => x.getClassLoader.getReference equals cref
	}

	def getIByteMethods(cha: ClassHierarchy) = {
		val appKlasses = cha filter iS(ClassLoaderReference.Application)
		appKlasses flatMap { _.getDeclaredMethods } collect { case m: IBytecodeMethod => m }
	}

	def getIClasses(cha: ClassHierarchy) = cha filter iS(ClassLoaderReference.Application)
}
