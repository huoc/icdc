package edu.illinois.wala

import scala.collection.JavaConversions.iterableAsScalaIterable
import edu.illinois.wala.Facade.InvokeI
import com.ibm.wala.types.ClassLoaderReference
import com.typesafe.config.ConfigFactory
import edu.illinois.wala.ssa.RichI
import edu.illinois.wala.Facade.C
import edu.illinois.wala.ipa.callgraph.AnalysisOptions
import com.ibm.wala.classLoader.IBytecodeMethod
import com.ibm.wala.ipa.callgraph.AnalysisCache
import com.ibm.wala.ipa.slicer.NormalStatement
import com.ibm.wala.ipa.callgraph.CallGraph
import edu.illinois.wala.ipa.callgraph.FlexibleCallGraphBuilder
import com.ibm.wala.ipa.callgraph.impl.Everywhere
import com.ibm.wala.ipa.slicer.Slicer
import com.ibm.wala.ipa.callgraph.impl.Util
import com.ibm.wala.analysis.typeInference.TypeInference
import java.io._
import com.ibm.wala.types.TypeReference
import collection.mutable.{ HashMap, MultiMap, Set }
import edu.udel.autoGen.BoundedView
import edu.udel.autoGen.ThinSlicer
import scalax.collection.Graph
import scalax.collection.GraphPredef._
import scalax.collection.GraphEdge.UnDiEdge

object test1 {
	val confFile = ConfigFactory.parseFile(new File("/home/huoc/research/facade/WALAFacade/bin/application.conf"))
                                                  //> confFile  : com.typesafe.config.Config = Config(SimpleConfigObject({"wala":
                                                  //| {"jre-lib-path":"/usr/lib/jvm/java-7-openjdk-amd64/jre/lib/rt.jar","depende
                                                  //| ncies":{"binary":${?wala.dependencies.binary}["/home/huoc/Projects/testassi
                                                  //| st/subjects/Employee/build/test/original"],"binary":${?wala.dependencies.bi
                                                  //| nary}["/home/huoc/Projects/testassist/subjects/Employee/build/app"],"jar":$
                                                  //| {?wala.dependencies.jar}["/home/huoc/Projects/testassist/subjects/lib/junit
                                                  //| -4.11.jar"],"jar":${?wala.dependencies.jar}["/home/huoc/Projects/testassist
                                                  //| /subjects/lib/hamcrest-all-1.3.jar"]},"entry":{"class":"LclassMutants/Emplo
                                                  //| yee/EmployeeTest","method":"main([Ljava/lang/String;)V"},"exclussions":""}}
                                                  //| ))
	implicit val config = ConfigFactory.load(confFile)
                                                  //> config  : com.typesafe.config.Config = Config(SimpleConfigObject({"os":{"ar
                                                  //| ch":"amd64","name":"Linux","version":"3.2.0-23-generic"},"awt":{"toolkit":"
                                                  //| sun.awt.X11.XToolkit"},"file":{"encoding":{"pkg":"sun.io"},"separator":"/"}
                                                  //| ,"path":{"separator":":"},"line":{"separator":"\n"},"java":{"vm":{"vendor":
                                                  //| "Oracle Corporation","name":"OpenJDK 64-Bit Server VM","specification":{"ve
                                                  //| ndor":"Oracle Corporation","name":"Java Virtual Machine Specification","ver
                                                  //| sion":"1.7"},"version":"22.0-b10","info":"mixed mode"},"home":"/usr/lib/jvm
                                                  //| /java-7-openjdk-amd64/jre","awt":{"graphicsenv":"sun.awt.X11GraphicsEnviron
                                                  //| ment","printerjob":"sun.print.PSPrinterJob"},"vendor":{"url":{"bug":"http:/
                                                  //| /bugreport.sun.com/bugreport/"}},"endorsed":{"dirs":"/usr/lib/jvm/java-7-op
                                                  //| enjdk-amd64/jre/lib/endorsed"},"library":{"path":"/usr/java/packages/lib/am
                                                  //| d64:/usr/lib/x86_64-linux-gnu/jni:/lib/x86_64-linux-gnu:/usr/lib/x86_64-lin
                                                  //| ux-gnu:/usr/lib/jni:/li
                                                  //| Output exceeds cutoff limit.
	val opts = AnalysisOptions()              //> true
                                                  //| opts  : edu.illinois.wala.ipa.callgraph.AnalysisOptions = edu.illinois.wala
                                                  //| .ipa.callgraph.AnalysisOptions@5f4f58dc
	import opts._




	//val mapping = (minits.toList ++ mms.toList).toMap
	val Employee = TypeReference.findOrCreate(ClassLoaderReference.Application, "LclassMutants/Employee/Employee")
                                                  //> Employee  : com.ibm.wala.types.TypeReference = <Application,LclassMutants/E
                                                  //| mployee/Employee>
	val String = TypeReference.findOrCreate(ClassLoaderReference.Application, "Ljava/lang/String")
                                                  //> String  : com.ibm.wala.types.TypeReference = <Application,Ljava/lang/String
                                                  //| >

	//  val solution = for ( a <- ans) yield (a.toSet &~ types.toSet)
	//  val du = cache getDefUse ir
	//   val ii = du.getDef(3)
	//    val node = cg getNode (test, Everywhere.EVERYWHERE)
	//    val nodes = cg getNodes test.getReference

	//    val ns = new NormalStatement(node, ir.getInstructions().size - 1)
	//    val paa = cgb.getPointerAnalysis()
	//    val slice = Slicer.computeBackwardSlice(ns, cg, paa)
}