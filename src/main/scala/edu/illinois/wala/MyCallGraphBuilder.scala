package edu.illinois.wala
import com.ibm.wala.ipa.callgraph.propagation._
import com.ibm.wala.ipa.cha.ClassHierarchy
import com.ibm.wala.ipa.callgraph.propagation.cfa.DefaultPointerKeyFactory
import scala.collection.JavaConverters._
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import com.ibm.wala.ssa.IRFactory
import com.ibm.wala.classLoader.IMethod
import com.ibm.wala.ssa.DefaultIRFactory
import edu.illinois.wala.ipa.callgraph.AbstractCallGraphBuilder
import edu.illinois.wala.ipa.callgraph.ExtraFeatures
import edu.illinois.wala.ipa.callgraph.AnalysisOptions
import com.ibm.wala.ipa.callgraph.AnalysisCache

object MyCallGraphBuilder {
	def apply()(implicit Config : Config): MyCallGraphBuilder =
	  apply(AnalysisOptions())
	
	def apply(options: AnalysisOptions): MyCallGraphBuilder = 
	  new MyCallGraphBuilder(options)
}

class MyCallGraphBuilder(
  val _cha: ClassHierarchy,
  val _options: AnalysisOptions,
  val _cache: AnalysisCache, pointerKeys: PointerKeyFactory)
  extends SSAPropagationCallGraphBuilder(_cha, _options, _cache, pointerKeys)
  with AbstractCallGraphBuilder with ExtraFeatures {

  def this(cha: ClassHierarchy, options: AnalysisOptions, irFactory: IRFactory[IMethod]) =
    this(cha, options, new AnalysisCache(irFactory), new DefaultPointerKeyFactory())

  def this(options: AnalysisOptions) = this(options.cha, options, new DefaultIRFactory())

  final lazy val heap = getPointerAnalysis().getHeapGraph()

  setContextInterpreter(theContextInterpreter)
  setContextSelector(cs)
  setInstanceKeys(instanceKeys)

  val cg = makeCallGraph(options)
  val cache = _cache

  // expose implicits
  implicit val implicitCha = cha
}